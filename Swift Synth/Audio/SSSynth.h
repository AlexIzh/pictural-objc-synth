//
//  SSSynth.h
//  Swift Synth
//
//  Created by Alex Severyanov on 12/1/20.
//  Copyright © 2020 Grant Emerson. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreGraphics/CoreGraphics.h>
#import "SSOscillator.h"

NS_ASSUME_NONNULL_BEGIN

@class SSSynth;

NS_SWIFT_NAME(SynthDelegate)
@protocol SSSynthDelegate
@optional
- (void)synthDidStartPlaying:(SSSynth *)synth;
- (void)synthDidEndPlaying:(SSSynth *)synth;
@end

NS_SWIFT_NAME(Synth)
@interface SSSynth : NSObject

@property (weak, nonatomic) id<SSSynthDelegate> delegate;

@property (class, readonly, strong) SSSynth *sharedSynth NS_SWIFT_NAME(shared);
@property (assign, nonatomic) float volume;

- (instancetype)initWithSignal:(SSSignal)signal;

- (void)setWaveformTo:(SSSignal)signal;

@end

NS_ASSUME_NONNULL_END

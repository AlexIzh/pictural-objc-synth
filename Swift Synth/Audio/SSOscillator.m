//
//  SSOscillator.m
//  Swift Synth
//
//  Created by Alex Severyanov on 12/1/20.
//  Copyright © 2020 Grant Emerson. All rights reserved.
//

#import "SSOscillator.h"

@implementation SSOscillator
static float _amplitude = 1;
static float _frequency = 440;

+ (float)amplitude {
    return _amplitude;
}

+ (void)setAmplitude:(float)amplitude {
    _amplitude = amplitude;
}

+ (float)frequency {
    return _frequency;
}

+ (void)setFrequency:(float)frequency {
    _frequency = frequency;
}

+ (SSSignal)sine {
    return ^(CGFloat time){
        return SSOscillator.amplitude * sin(2.0 * M_PI * SSOscillator.frequency * time);
    };
}

+ (SSSignal)triangle {
    return ^(CGFloat time){
        CGFloat period = 1.0 / SSOscillator.frequency;
        CGFloat currentTime = fmod(time, period);

        CGFloat value = currentTime / period;

        CGFloat result = 0.0;

        if (value < 0.25) {
            result = value * 4;
        }
        else if (value < 0.75) {
            result = 2.0 - (value * 4.0);
        }
        else {
            result = value * 4 - 4.0;
        }

        return SSOscillator.amplitude * result;
    };
}

+ (SSSignal)sawtooth {
    return ^(CGFloat time){
        CGFloat period = 1.0 / SSOscillator.frequency;
        CGFloat currentTime = fmod(time, period);

        return SSOscillator.amplitude * ((currentTime / period) * 2 - 1.0);
    };
}

+ (SSSignal)square {
    return ^(CGFloat time){
        CGFloat period = 1.0 / SSOscillator.frequency;
        CGFloat currentTime = fmod(time, period);

        return ((currentTime / period) < 0.5) ? SSOscillator.amplitude : -1.0 * SSOscillator.amplitude;
    };
}

+ (SSSignal)whiteNoise {
    return ^(CGFloat time){
        CGFloat random = ((CGFloat)arc4random() / UINT32_MAX) * 2.0 - 1.0;
        return SSOscillator.amplitude * random;
    };
}

@end

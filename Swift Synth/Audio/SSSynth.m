//
//  SSSynth.m
//  Swift Synth
//
//  Created by Alex Severyanov on 12/1/20.
//  Copyright © 2020 Grant Emerson. All rights reserved.
//

#import "SSSynth.h"
#import <AVFoundation/AVFoundation.h>

@interface SSSynth() {
    AVAudioSourceNode *_sourceNode;
}

@property (strong, nonatomic) AVAudioEngine *audioEngine;

@property (assign, nonatomic) CGFloat time;
@property (assign, nonatomic) CGFloat sampleRate;
@property (assign, nonatomic) CGFloat deltaTime;

@property (copy, nonatomic) SSSignal signal;

@end

@implementation SSSynth

+ (instancetype)sharedSynth {
    static SSSynth *sharedSynth = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedSynth = [[self alloc] init];
    });
    return sharedSynth;
}

- (float)volume {
    return self.audioEngine.mainMixerNode.outputVolume;
}

- (void)setVolume:(float)volume {
    self.audioEngine.mainMixerNode.outputVolume = volume;
    if (volume) {
        [self.delegate synthDidStartPlaying:self];
    } else {
        [self.delegate synthDidEndPlaying:self];
    }
}

- (instancetype)init {
    return [self initWithSignal:SSOscillator.sine];
}

- (instancetype)initWithSignal:(SSSignal)signal {
    if (self = [super init]) {
        self.audioEngine = [AVAudioEngine new];

        __auto_type mainMixer = self.audioEngine.mainMixerNode;
        __auto_type outputNode = self.audioEngine.outputNode;
        __auto_type format = [outputNode inputFormatForBus: 0];

        self.sampleRate = (CGFloat)format.sampleRate;
        self.deltaTime = 1 / self.sampleRate;

        self.signal = signal;

        __auto_type inputFormat = [[AVAudioFormat alloc] initWithCommonFormat:format.commonFormat
                                                                   sampleRate:self.sampleRate
                                                                     channels:1
                                                                  interleaved:format.isInterleaved];

        [self.audioEngine attachNode:self.sourceNode];
        [self.audioEngine connect:self.sourceNode to:mainMixer format:inputFormat];
        [self.audioEngine connect:mainMixer to:outputNode format:nil];
        mainMixer.outputVolume = 0;

        NSError *error = nil;
        if (![self.audioEngine startAndReturnError:&error]) {
            NSLog(@"Could not start audioEngine: %@", error.localizedDescription);
        }
    }
    return self;
}

- (void)setWaveformTo:(SSSignal)signal {
    self.signal = signal;
}

- (AVAudioSourceNode *)sourceNode {
    if (!_sourceNode) {
        // Swift version contains retain-cycle here, interesting if it's intentionally
        __weak typeof(self) weakSelf = self;
        _sourceNode = [[AVAudioSourceNode alloc] initWithRenderBlock:^OSStatus(BOOL * _Nonnull isSilence, const AudioTimeStamp * _Nonnull timestamp, AVAudioFrameCount frameCount, AudioBufferList * _Nonnull outputData) {

            typeof(weakSelf) strongSelf = weakSelf;
            __auto_type ablPointer = outputData;

            for (int frame = 0; frame < frameCount; frame++) {

                __auto_type sampleVal = strongSelf.signal(strongSelf.time);
                strongSelf.time += strongSelf.deltaTime;


                for (int index = 0; index < ablPointer->mNumberBuffers; index++) {
                    float *buffer = (float *)ablPointer->mBuffers[index].mData;
                    buffer[frame] = sampleVal;
                }
            }
            return noErr;
        }];
    }
    return _sourceNode;
}

@end

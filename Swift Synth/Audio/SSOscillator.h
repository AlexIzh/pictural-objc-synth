//
//  SSOscillator.h
//  Swift Synth
//
//  Created by Alex Severyanov on 12/1/20.
//  Copyright © 2020 Grant Emerson. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreGraphics/CoreGraphics.h>

typedef CGFloat (^SSSignal)(CGFloat) NS_SWIFT_NAME(Signal);

typedef NS_CLOSED_ENUM(NSInteger, SSWaveform) {
    sine, triangle, sawtooth, square, whiteNoise
} NS_SWIFT_NAME(Waveform);

NS_ASSUME_NONNULL_BEGIN

NS_SWIFT_NAME(Oscillator)
@interface SSOscillator : NSObject

@property (class, assign, nonatomic) float amplitude;
@property (class, assign, nonatomic) float frequency;

@property (class, copy, readonly, nonatomic) SSSignal sine;
@property (class, copy, readonly, nonatomic) SSSignal triangle;
@property (class, copy, readonly, nonatomic) SSSignal sawtooth;
@property (class, copy, readonly, nonatomic) SSSignal square;
@property (class, copy, readonly, nonatomic) SSSignal whiteNoise;

@end

NS_ASSUME_NONNULL_END
